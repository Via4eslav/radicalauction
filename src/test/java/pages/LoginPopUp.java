package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 20.09.2017.
 */
public class LoginPopUp {
    WebDriver driver;

    public LoginPopUp (WebDriver driver){
        this.driver = driver;
    }

//    Locators

    By popUpLogin = By.cssSelector(".button.js-mfp-ajax");
    By userLogin = By.xpath("html/body/div[2]/div/div[1]/div/div/div[1]/div/div/div[1]/div/input");
    By password = By.name("password");
    By enterButton = By.cssSelector(".button.button--sizeLg.js-form-submit");

    public void setPopUpLogin() {
        driver.findElement(popUpLogin).click();
    }

    public void setUserLogin(String strUserLogin) {
        driver.findElement(userLogin).sendKeys(strUserLogin);
    }

    public void setUserPassword(String strUserPassword) {
        driver.findElement(password).sendKeys(strUserPassword);
    }

    public void setEnterButton(){
        driver.findElement(enterButton).click();
    }

    public void setPreconditions(String strUserLogin, String strUserPassword){
        this.setPopUpLogin();
        this.setUserLogin(strUserLogin);
        this.setUserPassword(strUserPassword);
        this.setEnterButton();
    }

}
