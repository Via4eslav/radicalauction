package pages.Profile;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.Random;

/**
 * Created by ivanov.v on 20.09.2017.
 */

public class AuctionsCreateForm {
    WebDriver driver;

    public AuctionsCreateForm(WebDriver driver){
        this.driver = driver;
    }

//    Locators

    By nameFieldRu = By.cssSelector("#name_ru");
    By nameFieldEng = By.cssSelector("#name_eng");
    By descriptionRu = By.cssSelector("#description_ru");
    By descriptionEng = By.cssSelector("#description_eng");
    By addsRu = By.cssSelector("#ad_ru");
    By addsEng = By.cssSelector("#ad_eng");
    By urlField = By.cssSelector("#url");
    By startPrice = By.cssSelector("#costStart");
    By percentTrend = By.cssSelector("#costTrend");
    By stopPrice = By.cssSelector("#costStop");
    By buyerPaysAll = By.xpath("html/body/div[1]/main/section/div/div/div[10]/div/div/label[1]/ins");
    By additionalInfoFieldRu = By.xpath("html/body/div/main/section/div/div/div[12]/div[2]/div[2]/div/div/textarea");
    By additionalInfoFieldEng = By.xpath("html/body/div[1]/main/section/div/div/div[12]/div[3]/div[2]/div/div/textarea");
    By inputFile = By.id("file_multiple");
    By optionRules = By.xpath("html/body/div/main/section/div/div/div[16]/div/div/label/ins");
    By saveButton = By.xpath("html/body/div[1]/main/section/div/div/div[17]/div[3]/div/button");

//    Переделать на переход методом навигации по личному кабинету.
    public void navigateToAuctionCreatePage(){
        driver.navigate().to("http://rau24.wezom.net/auctions/create");
    }

    public void userFillsNameFieldRu(String strNameFieldRu){
        Random r = new Random();
        char c = (char)(r.nextInt(26) + 'a');
        driver.findElement(nameFieldRu).sendKeys(strNameFieldRu + " " + c);
    }

    public void userFillsNameFieldEng(String strNameFieldEng){
        Random r = new Random();
        char c = (char)(r.nextInt(26) + 'a');
        driver.findElement(nameFieldEng).sendKeys(strNameFieldEng + " " + c);
    }

    public void userFillsDescriptionRu(String strDescriptionRu){
        driver.findElement(descriptionRu).sendKeys(strDescriptionRu);
    }

    public void userFillsDescriptionEng(String strDescriptionEng){
        driver.findElement(descriptionEng).sendKeys(strDescriptionEng);
    }

    public void userFillsAddsRu(String strAddsRu){
        driver.findElement(addsRu).sendKeys(strAddsRu);
    }

    public void userFillsAddsEng(String strAddsEng){
        driver.findElement(addsEng).sendKeys(strAddsEng);
    }

    public void userFillsUrlField(String strUrlField){
        driver.findElement(urlField).sendKeys(strUrlField);
    }

    public void userSetStartPrice(String strStartPrice){
        driver.findElement(startPrice).sendKeys(strStartPrice);
    }

    public void userSetPercentTrend(String strPercentTrennd){
        driver.findElement(percentTrend).sendKeys(strPercentTrennd);
    }

    public void userSetStopPrice(String strStopPrice){
        driver.findElement(stopPrice).sendKeys(strStopPrice);
    }

    public void userCheckOptionBuyerPaysAll(){
        driver.findElement(buyerPaysAll).click();
    }

    public void userFillsAdditionalInfoRu(String strAdditionalInfoRu){
        driver.findElement(additionalInfoFieldRu).sendKeys(strAdditionalInfoRu);
    }

    public void userFillsAdditionalInfoEng(String strAdditionalInfoEng){
        driver.findElement(additionalInfoFieldEng).sendKeys(strAdditionalInfoEng);

    }

    public void userUploadFile () {
        driver.findElement(inputFile).sendKeys("C:/test.png");
    }

    public void userCheckRulesOption(){
        driver.findElement(optionRules).click();
    }

    public void userClickOnSaveButton(){
        driver.findElement(saveButton).click();
    }

}
