package pages.Profile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 22.09.2017.
 */
public class UserLogOut {
    WebDriver driver;

    public UserLogOut(WebDriver driver){
        this.driver = driver;
    }

//    Locators
//    dropdown locators
    By profileDropDownMenu = By.cssSelector(".button.button--bg.button--arr");
    By logOutLink = By.xpath("html/body/div[1]/div[1]/header/div[2]/div/div/div[3]/div[1]/div[2]/div[2]/ul/li[6]/a");
    By itemMyAcution = By.xpath("html/body/div[1]/div[1]/header/div[2]/div/div/div[3]/div[1]/div[2]/div[2]/ul/li[1]/a");

//    profile page locators

    By exitLink = By.xpath("html/body/div[1]/main/section/div/div/div[1]/div/div[2]/div/div/ul/li[6]/a");

//      User log out by dropdown menu

    public void setProfileDropDownMenu(){
        driver.findElement(profileDropDownMenu).click();
    }

    public void setLogOutLink(){
        driver.findElement(logOutLink).click();
    }

//    User log out on profile page

    public void setUserClickOnMenuItem(){
        driver.findElement(itemMyAcution).click();
    }

    public void setUserClickOnExitLink(){
        driver.findElement(exitLink).click();
    }


}
