package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 20.09.2017.
 */
public class SubscribeForm {
    WebDriver driver;

    public SubscribeForm(WebDriver driver){
        this.driver = driver;
    }

//    Locators

           By subscribeForm = By.id("email");
           By subscribeBtn = By.cssSelector(".button.button--full.js-form-submit");
    /**
     *
     * @param email - user valid email
     */

    public void fillSubscribeField(String email) {
        driver.findElement(subscribeForm).sendKeys(email);
    }

    public void clickSubscribeBtn() {
        driver.findElement(subscribeBtn).click();
    }
}
