package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPopUp;
import pages.Profile.AuctionsCreateForm;

/**
 * Created by ivanov.v on 21.09.2017.
 */
public class UserCreateOpenAuction extends BaseConfig {

    public LoginPopUp userLogining;

    public AuctionsCreateForm  userFillsAuctionForm;

    @BeforeMethod
    public void beforeTest(){
        userLogining = new LoginPopUp(driver);
        userFillsAuctionForm = new AuctionsCreateForm(driver);
    }

    @Test
    public void setUserFillsAuctionForm() throws InterruptedException {
        userLogining.setPreconditions("ivanov.v.wezom@gmail.com", "123456");
        Thread.sleep(3000);
        userFillsAuctionForm.navigateToAuctionCreatePage();
        userFillsAuctionForm.userFillsNameFieldRu("Тестовый аукцион");
        userFillsAuctionForm.userFillsNameFieldEng("Test auction");
        userFillsAuctionForm.userFillsDescriptionRu("Лорэм ипсум сит амэт долор.");
        userFillsAuctionForm.userFillsDescriptionEng("Lorem ipsum sit amet dolot.");
        userFillsAuctionForm.userFillsAddsRu("Продаётся гараж");
        userFillsAuctionForm.userFillsAddsEng("Garage is selling right now");
        userFillsAuctionForm.userFillsUrlField("http://rau24.wezom.net/");
        userFillsAuctionForm.userSetStartPrice("3000");
        userFillsAuctionForm.userSetPercentTrend("1");
        userFillsAuctionForm.userSetStopPrice("2670");
        userFillsAuctionForm.userCheckOptionBuyerPaysAll();
        userFillsAuctionForm.userFillsAdditionalInfoRu("Дополнительный Лорэм ипсум сит амэт долор.");
        userFillsAuctionForm.userFillsAdditionalInfoEng("Additional Lorem ipsum sit amet dolot.");
        userFillsAuctionForm.userUploadFile();
        userFillsAuctionForm.userCheckRulesOption();
        userFillsAuctionForm.userClickOnSaveButton();

    }

}
