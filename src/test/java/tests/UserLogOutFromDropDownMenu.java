package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPopUp;
import pages.Profile.UserLogOut;

/**
 * Created by ivanov.v on 22.09.2017.
 */
public class UserLogOutFromDropDownMenu extends BaseConfig {
    public LoginPopUp userLogining;
    public UserLogOut userLogOutFromDropDown;

    @BeforeMethod
    public void beforeTest(){
        userLogining = new LoginPopUp(driver);
        userLogOutFromDropDown = new UserLogOut(driver);
    }

    @Test
    public void userClickDropDownMenu() {
        userLogining.setPreconditions("ivanov.v.wezom@gmail.com", "123456");
        userLogOutFromDropDown.setProfileDropDownMenu();
        userLogOutFromDropDown.setLogOutLink();
//        Добавить проверку
    }

}
