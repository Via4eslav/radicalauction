package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPopUp;
import pages.Profile.UserLogOut;

/**
 * Created by ivanov.v on 22.09.2017.
 */
public class UserLogOutFromProfilePage extends BaseConfig {
    public LoginPopUp userLogining;
    public UserLogOut userLogOutFromProfilePage;

    @BeforeMethod
    public void beforeTest(){
        userLogining = new LoginPopUp(driver);
        userLogOutFromProfilePage = new UserLogOut(driver);
    }

    @Test
    public void setUserLogOutFromProfilePage(){
        userLogining.setPreconditions("ivanov.v.wezom@gmail.com", "123456");
        userLogOutFromProfilePage.setProfileDropDownMenu();
        userLogOutFromProfilePage.setUserClickOnMenuItem();
        userLogOutFromProfilePage.setUserClickOnExitLink();
    }

}
