package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.SubscribeForm;

public class UserSubscribeTest extends BaseConfig {

    public SubscribeForm userSubscribe;

    @BeforeMethod
    public void beforeTest(){
        userSubscribe = new SubscribeForm(driver);
    }

    @Test
    public void startTest() throws InterruptedException {
        userSubscribe.fillSubscribeField("test@test.ru");
        userSubscribe.clickSubscribeBtn();
    }

//    Необходимо дописать Assert


}
